# Testing CWL workflows with singularity on LSF

This repository contains tests for running cwltool, cwltoil, and cwlexec in combination with singularity on an LSF cluster.  

The test scripts are in `./test_jobs_toil` and `./test_jobs_cwlexec`. Change into the respective subdirectory before executing them. Also make sure that the applied runner is installed and available in the PATH (see below).

All CWL workflows and CWL tool wrappers which are used in the tests are located in `./CWL`. The YAML input files can be found in `./test_input`. For some tests, small synthetic input data is needed. It is located in `./test_data`.

## Installation of cwl runners:
### Toil:
Toil version 3.18.0 has a singularity-specific issue, which has already been fixed but not not released yet (https://github.com/DataBiosphere/toil/issues/2470). Until this fix makes it into a new release, version 3.17.0 can be used.
  
Please note:  
Toil must be available to all computing nodes, not just the submit node. The easiest way to achive this is to install it in a shared directory. Then make it available in the PATH on the submit node. The PATH will be automatically forwarded by bsub to worker nodes.
  
Toil can be installed from the conda environment recipe which can be found in `./conda_toil`:
```
conda env create -f ./conda_envs/conda_toil.yml
```
To make it available in path, you can either source activate it (`source activate conda_toil`), or you add the environment's bin directory to the PATH (e.g. `export PATH="/home/${USER}/.conda/envs/conda_toil/bin:$PATH"`).
  
Of course you can also install it without conda by using pip:
```
pip install toil[cwl]==3.17.0
```
Attention: The `[cwl]` part is important to include all the needed dependencies (e.g. cwltool). Moreover, make sure that you install into python2.7 not 3. (Cross-compatibility will start with toil 3.18.0.)  
For evaluating JavaScript expressions inside CWL, you have to install node.js, too (is already included in the above conda environment).

### cwlexec
I just downloaded the latest version, unpacked it, and added it to the path:

```
wget https://github.com/IBMSpectrumComputing/cwlexec/releases/download/v0.2.1/cwlexec-0.2.1.tar.gz
tar xzvf cwlexec-0.2.1.tar.gz
export PATH="$HOME/cwlexec-0.2.1:$PATH"
```

Note: no test for cwlexec with singularity yet.