#!/bin/bash
CWLSCRIPT="../CWL/workflows/wf_touch.cwl"
INPUT_YAML="../test_input/touch.yaml"
OUTDIR="${HOME}/test_out/"

BASEDIR="${HOME}/test_tmp/base"
WORKDIR="${HOME}/test_tmp/work"
TMPDIR="${HOME}/test_tmp/tmp"
TMPOUTDIR="${HOME}/test_tmp/tmp_out"

mkdir -p "$OUTDIR" "$BASEDIR" "$WORKDIR" "$TMPDIR" "$TMPOUTDIR" 

cwltool --debug --singularity \
	--tmp-outdir-prefix "$TMPOUTDIR" --tmpdir-prefix "$TMPDIR" \
	--basedir "$BASEDIR" --outdir "$OUTDIR" \
	"$CWLSCRIPT" "$INPUT_YAML"