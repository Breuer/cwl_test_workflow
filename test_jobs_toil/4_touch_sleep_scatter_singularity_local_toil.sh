#!/bin/bash
CWLSCRIPT="../CWL/workflows/wf_touch_sleep_scatter.cwl"
INPUT_YAML="../test_input/touch_sleep_scatter.yaml"
OUTDIR="${HOME}/test_out/"

BASEDIR="${HOME}/test_tmp/base"
WORKDIR="${HOME}/test_tmp/work"
TMPDIR="${HOME}/test_tmp/tmp"
TMPOUTDIR="${HOME}/test_tmp/tmp_out"

mkdir -p "$OUTDIR" "$BASEDIR" "$WORKDIR" "$TMPDIR" "$TMPOUTDIR" 

cwltoil --singularity --batchSystem singleMachine --logDebug --clean always --cleanWorkDir always \
	--tmp-outdir-prefix "$TMPOUTDIR" --tmpdir-prefix "$TMPDIR" \
	--workDir "$WORKDIR" --basedir "$BASEDIR" --outdir "$OUTDIR" \
	"$CWLSCRIPT" "$INPUT_YAML"