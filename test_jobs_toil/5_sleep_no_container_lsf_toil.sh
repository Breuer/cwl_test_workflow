#!/bin/bash
CWLSCRIPT="../CWL/workflows/wf_sleep_no_container.cwl"
INPUT_YAML="../test_input/sleep.yaml"
OUTDIR="${HOME}/test_out/"

BASEDIR="${HOME}/test_tmp/base"
WORKDIR="${HOME}/test_tmp/work"
TMPDIR="${HOME}/test_tmp/tmp"
TMPOUTDIR="${HOME}/test_tmp/tmp_out"
JOBSTORE_BASE="${HOME}/test_tmp/"

rm -r "$OUTDIR" "$BASEDIR" "$WORKDIR" "$TMPDIR" "$TMPOUTDIR" "$JOBSTORE_BASE"
mkdir -p "$OUTDIR" "$BASEDIR" "$WORKDIR" "$TMPDIR" "$TMPOUTDIR" "$JOBSTORE_BASE"

cwltoil --batchSystem=lsf --disableCaching --logDebug --clean always --cleanWorkDir always \
	--retryCount 0 \
	--workDir "$WORKDIR" --basedir "$BASEDIR" --outdir "$OUTDIR" \
	--jobStore "$JOBSTORE_BASE/jobstore" \
	"$CWLSCRIPT" "$INPUT_YAML"