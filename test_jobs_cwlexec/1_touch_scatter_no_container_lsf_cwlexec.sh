#!/bin/bash
CWLSCRIPT="../CWL/workflows/wf_touch_scatter_no_container.cwl"
INPUT_YAML="../test_input/touch_scatter.yaml"
OUTDIR="${HOME}/test_out/"

BASEDIR="${HOME}/test_tmp/base"
WORKDIR="${HOME}/test_tmp/work"
TMPDIR="${HOME}/test_tmp/tmp"
TMPOUTDIR="${HOME}/test_tmp/tmp_out"

mkdir -p "$OUTDIR" "$BASEDIR" "$WORKDIR" "$TMPDIR" "$TMPOUTDIR" 

cwlexec --debug \
	--workdir "$WORKDIR" --outdir "$OUTDIR" \
	"$CWLSCRIPT" "$INPUT_YAML"